# Day-10 :
----------

## Topic of the Day: High-throughput computing with the AiiDA platform


This directory contains materials for the AiiDA hands-on, plus possible addictional exercises. 

Have fun !!!

### Tutorial material and instructions

The tutorial instructions will be available on the official AiiDA tutorials website:
https://aiida-tutorials.readthedocs.io/en/latest/
where a specific section for this tutorial will be available.

For a preview, you can check section 2: "A first taste" of the 2019 Xiamen tutorial, that you
can find on the website above (or you can reach it directly via
[this URL](https://aiida-tutorials.readthedocs.io/en/latest/pages/2019_Xiamen/sections/first_taste.html)).
