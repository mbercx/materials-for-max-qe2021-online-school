# Topics of Day-9 hands-on session

Parallel execution on CPUs and GPUs.
How to optimize CPU only runs and how to efficiently run on accelerated systems.

------------------------------------------------------------------------

**Exercise 0:** Basic info about GPU acceleration.

    cd example0.intro/

**Exercise 1:** Setting up QE on CPU and GPU systems.

    cd example1.setup/

**Exercise 2:** Parallel options -- improve performance with npool and ndiag

    cd example2.CPU/

**Exercise 3:** Accelerated systems -- how to run with NVidia GPUs

    cd example3.GPU/
