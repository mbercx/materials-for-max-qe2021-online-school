# Day-6 :
---------

### Topic of the day: Time-dependent density-functional-perturbation-theory (TDDFPT)


**Exercise 1:** Calculation of the absorption spectrum of benzene molecule (C6H6) using the Independent Particle Approximation

    cd example1/

**Exercise 2:** Calculation of the absorption spectrum of benzene molecule (C6H6) using the `turbo_davidson.x` code

    cd example2/

**Exercise 3:** Calculation of the absorption spectrum of benzene molecule (C6H6) using the `turbo_lanczos.x` code

    cd example3/

**Exercise 4:** Calculation of the absorption spectrum of methane molecule (CH4) with hybrid pseudo-potential using the `turbo_davidson.x` code

    cd example4/
